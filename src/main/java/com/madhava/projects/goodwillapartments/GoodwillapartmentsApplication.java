package com.madhava.projects.goodwillapartments;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoodwillapartmentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoodwillapartmentsApplication.class, args);
	}
}
